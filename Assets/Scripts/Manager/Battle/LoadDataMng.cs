﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;
using UnityEngine.UI;

public class LoadDataMng : MonoBehaviour
{
    [Header("ID")]
    public int enemyId;
    public int playerId;

    [Header("Level")]
    public int enemyLv;
    public int playerLv;

    [Header("Img")]
    public Sprite enemySpr;
    public Sprite playerSpr;
    public Image enemyImg;
    public Image playerImg;

    public PkmInBattle enemy;
    public PkmInBattle player;
    public GameObject enemyObj;
    public GameObject playerObj;

    [Header("Abilities")]
    public AbilitiesInBattle abilitiesInBattle;

    public void SetUpPkmInfor(int id,int level ,PkmInfor pkmInfor)
    {
        pkmInfor.id = id;
        pkmInfor.level = level;

        //load name
        pkmInfor.name = NamePkm_DT.Instance.NamePkm[id];
        // load Type
        pkmInfor.typePkm.TypePokemon = TypePkm_DT.Instance.TypePkms[id].TypePokemon;
        //Loat Base Stats
        pkmInfor.statsPkm.SetUpData(BaseStatsPkm_DT.Instance.BaseStatsPkms[id]);

        #region Abilities
        pkmInfor.SetUpAbilities(AbilitiesPkm_DT.Instance.GetAbilitiesPkm(pkmInfor.name).Abilities);
        #endregion


        #region Nature
        pkmInfor.naturePkm = (NaturePkmEnum)(Random.Range(0, 25));
        NaturePkm naturePkm;
        NaturePkm_DT.Instance.NaturePkms.TryGetValue(pkmInfor.naturePkm.ToString(), out naturePkm);
        NaturePkm_DT.Instance.DoNatureUp(ref pkmInfor, naturePkm.upStats);
        NaturePkm_DT.Instance.DoNatureDown(ref pkmInfor, naturePkm.downStats);
        #endregion

        //IVs
        pkmInfor.IVs = 1;//Random.Range(0, 32);

        #region MovesPkm
        List<string> st= MovesPkm_DT.Instance.GetListMoveCanLearnBaseOnCurrenLevel(pkmInfor.name, pkmInfor.level);
        pkmInfor.moveInfors = new MoveInfor[4];
        for(int i=0;i<4;i++)
        {
            pkmInfor.moveInfors[i] = new MoveInfor();
        }
        int countMove = 0;
        for(int i=st.Count-1;i>=0;i--)
        {
            if (countMove >= 4) break;
            pkmInfor.moveInfors[countMove] = new MoveInfor();
            pkmInfor.moveInfors[countMove].SetUpData(AllListMoves.Instance.GetMoveInfor(st[i]));
            countMove += 1;
        }
        #endregion

    }

    void CreatePlayerAbilities()
    {
        if (abilitiesInBattle.playerAbi == null) abilitiesInBattle.playerAbi = new List<IAbility>();
        for (int i = 0; i < player.PkmInfor.abilities.Count; i++)
        {
            IAbility ab= FactoryAbilities.Instance.CreateAbility(player.PkmInfor.abilities[i], playerObj);
            abilitiesInBattle.playerAbi.Add(ab);
        }
    }

    void CreateEnemyAbilities()
    {
        if (abilitiesInBattle.enemyAbi == null) abilitiesInBattle.enemyAbi = new List<IAbility>();
        for (int i = 0; i < enemy.PkmInfor.abilities.Count; i++)
        {
            IAbility ab = FactoryAbilities.Instance.CreateAbility(enemy.PkmInfor.abilities[i], enemyObj);
            abilitiesInBattle.enemyAbi.Add(ab);
        }
    }

    public void LoadDataStart()
    {
        SetUpPkmInfor(playerId,playerLv,player.PkmInfor);
        SetUpPkmInfor(enemyId, enemyLv,enemy.PkmInfor);
        CreatePlayerAbilities();
        CreateEnemyAbilities();

        playerImg.sprite = playerSpr;
        enemyImg.sprite = enemySpr;

        // caculation HP
        enemy.PkmInforInBattle.HP = CalculationFormulaBattleManager.Instance.CountHP(enemy.PkmInfor);
        player.PkmInforInBattle.HP = CalculationFormulaBattleManager.Instance.CountHP(player.PkmInfor);

        string playerID = ConvertNameSprite(playerId) + "b";
        playerImg.sprite = LoadSpritePkmMng.Instance.GetSpriteByID(playerID);

    }

    string ConvertNameSprite(int id)
    {
        id += 1;
        string s = "";
        if(id>=0 && id<10)
        {
            s = "00" + id;
            return s;
        }
        else if(id>=10 && id <100)
        {
            s = "0" + id;
            return s;
        }
        s += id;
        return s;
    }
}
