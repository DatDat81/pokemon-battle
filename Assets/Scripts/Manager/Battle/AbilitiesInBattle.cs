﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbilitiesInBattle :MonoBehaviour
{
    public List<IAbility> enemyAbi = new List<IAbility>();
    public List<IAbility> playerAbi = new List<IAbility>();

}
