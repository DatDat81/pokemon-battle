﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : Singleton<BattleManager>
{
    [SerializeField] private LoadDataMng dataMng;
    [SerializeField] private UIFrameBattleMng uIFrameBattleMng;
    
    [SerializeField] private WeatherEnum weather;

    [Header("Pkm Infor")] //bien tam thoi dung trong tinh toan va su dung hien tai trong tran dau
    [SerializeField] private PkmInfor enemyInforCurr; // current
    [SerializeField] private PkmInfor playerInforCurr;
    [SerializeField] private int enemyHpCurr;
    [SerializeField] private int playerHpCurr;

    [Header("Move Of Player And Enemy Used")]
    private string enemyMoveLastUsed;
    [SerializeField]
    private string playerMoveLastUsed;

    public BattleThreadEnum battleThread;

    public PkmInfor EnemyInforCurr { get => enemyInforCurr; set => enemyInforCurr = value; }

    public int EnemyHpCurr { get => enemyHpCurr; set => enemyHpCurr = value; }
    public int PlayerHpCurr { get => playerHpCurr; set => playerHpCurr = value; }
    public PkmInfor PlayerInforCurr { get => playerInforCurr; set => playerInforCurr = value; }
    public LoadDataMng DataMng { get => dataMng; set => dataMng = value; }
    public string EnemyMoveLastUsed { get => enemyMoveLastUsed; set => enemyMoveLastUsed = value; }
    public string PlayerMoveLastUsed { get => playerMoveLastUsed; set => playerMoveLastUsed = value; }
    public WeatherEnum Weather { get => weather; set => weather = value; }
    public UIFrameBattleMng UIFrameBattleMng { get => uIFrameBattleMng; set => uIFrameBattleMng = value; }

    void Start()
    {
        dataMng.LoadDataStart();
        uIFrameBattleMng.ShowMoveText(dataMng.player.PkmInfor.moveInfors[0].name, dataMng.player.PkmInfor.moveInfors[1].name, dataMng.player.PkmInfor.moveInfors[2].name, dataMng.player.PkmInfor.moveInfors[3].name);

        uIFrameBattleMng.UpdatePlayerInfor(dataMng.player);
        uIFrameBattleMng.UpdateEnemyInfor(dataMng.enemy);

        enemyInforCurr.SetUpData(dataMng.enemy.PkmInfor);
        playerInforCurr.SetUpData(dataMng.player.PkmInfor);
        enemyHpCurr = dataMng.enemy.PkmInforInBattle.HP;
        playerHpCurr = dataMng.player.PkmInforInBattle.HP;

        //test
        battleThread = BattleThreadEnum.StartTurn;

    }

    private void Update()
    {
        BattleThread();

        if (playerHpCurr < 0) playerHpCurr = 0;
        if (playerHpCurr > dataMng.player.PkmInforInBattle.HP) playerHpCurr = dataMng.player.PkmInforInBattle.HP;
        if (enemyHpCurr < 0) enemyHpCurr = 0;
        if (enemyHpCurr > dataMng.enemy.PkmInforInBattle.HP) enemyHpCurr = dataMng.enemy.PkmInforInBattle.HP;

        //test
        uIFrameBattleMng.UpdatePlayerHp(playerHpCurr, dataMng.player.PkmInforInBattle.HP);
        uIFrameBattleMng.UpdateEnemyHp(enemyHpCurr, dataMng.enemy.PkmInforInBattle.HP);
    }

    void BattleThread() //// chia lai thread theo ability intefface
    {
        if (battleThread == BattleThreadEnum.StartBattle)
        {
            StartBattle();
            battleThread = BattleThreadEnum.WaitStatus;
        }
        else if (battleThread == BattleThreadEnum.StartTurn)
        {
            StartTurn();
            //battleThread = BattleThreadEnum.WaitStatus;
        }
        else if (battleThread == BattleThreadEnum.BeforeAttack)
        {
            BeforeAttackTurn();

            // test player attack
            MoveInfor moveInfor = AllListMoves.Instance.GetMoveInfor(playerMoveLastUsed);
            int dmg = CalculationFormulaBattleManager.Instance.GetDamage(moveInfor, playerInforCurr, enemyInforCurr, dataMng.player.PkmInforInBattle, this);
            enemyHpCurr -= dmg;
            uIFrameBattleMng.UpdateEnemyHp(enemyHpCurr, dataMng.enemy.PkmInforInBattle.HP);
            UIFrameBattleMng.ShowNoty("Damage: " + dmg + ". " + CalculationFormulaBattleManager.Instance.notyText +". Weather: "+weather);
            Debug.Log(dmg);
            //test
            battleThread = BattleThreadEnum.EndTurn;

            //battleThread = BattleThreadEnum.WaitStatus;
        }
        else if (battleThread == BattleThreadEnum.InAttack)
        {
            AttackTurn();
            battleThread = BattleThreadEnum.WaitStatus;
        }
        else if (battleThread == BattleThreadEnum.AfterAttack)
        {
            AfterAttackTurn();
            battleThread = BattleThreadEnum.WaitStatus;
        }
        else if (battleThread == BattleThreadEnum.EndTurn)
        {
            EndTurn();

            //test
            battleThread = BattleThreadEnum.StartTurn;
        }
        else if (battleThread == BattleThreadEnum.ChangePkmInTurn)
        {
            ChangePkmInTurn();
            battleThread = BattleThreadEnum.WaitStatus;
        }
        else if (battleThread == BattleThreadEnum.ChangePkmOutTurn)
        {
            ChangePkmOutTurn();
            battleThread = BattleThreadEnum.WaitStatus;
        }
        else if (battleThread == BattleThreadEnum.EndBattle)
        {
            EndBattle();
            battleThread = BattleThreadEnum.WaitStatus;
        }
    }
    void StartBattle()
    {
        uIFrameBattleMng.UpdateStatusPlayer(StatusPkmEnum.Normal);
        uIFrameBattleMng.UpdateStatusEnemy(StatusPkmEnum.Normal);
        foreach(var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnStartBattle(this);
        }
        //dataMng.enemy.GetComponent<IAbility>().OnStartBattle(this);
    }     
    
    void StartTurn()
    {
        foreach (var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnStartTurn(this);
        }
        //dataMng.enemy.GetComponent<IAbility>().OnStartTurn(this);
    }

    void BeforeAttackTurn()
    {
        foreach (var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnBeforeAttack(this);
        }
        //dataMng.enemy.GetComponent<IAbility>().OnBeforeAttack(this);
    }

    void AttackTurn()
    {

    }

    void AfterAttackTurn()
    {
        foreach (var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnAfterAttack(this);
            ab.OnAfterChangeStatus(this);
        }
        //dataMng.enemy.GetComponent<IAbility>().OnAfterAttack(this);
    }

    void EndTurn()
    {
        foreach (var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnEndBattle(this);
        }
        ///dataMng.enemy.GetComponent<IAbility>().OnEndTurn(this);

        EnemyMoveLastUsed = "";
        PlayerMoveLastUsed = "";
    }

    void ChangePkmInTurn()
    {
        foreach (var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnChangePokemonIn(this);
        }
        //dataMng.enemy.GetComponent<IAbility>().OnChangePokemonIn(this);
    }

    void ChangePkmOutTurn()
    {
        foreach (var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnChangePokemonOut(this);
        }
        //dataMng.enemy.GetComponent<IAbility>().OnChangePokemonOut(this);
    }

    void EndBattle()
    {
        foreach (var ab in dataMng.abilitiesInBattle.playerAbi)
        {
            ab.OnEndBattle(this);
        }
        //dataMng.enemy.GetComponent<IAbility>().OnEndBattle(this);
    }
}
