﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIFrameBattleMng : Singleton<UIFrameBattleMng>
{
    [Header("Move Text")]
    public Text move1Text;
    public Text move2Text;
    public Text move3Text;
    public Text move4Text;

    [Header("Enemy Infor")]
    public Text enemyName;
    public Text enemyLevel;
    public GameObject enemyHealthBar;

    [Header("Player Infor")]
    public Text playerName;
    public Text playerLevel;
    public Text playerHp;
    public GameObject playerHealthBar;

    [Header("Notifycation")]
    public GameObject notiObj;
    public Text notiText;

    [Header("Status Pkm")]
    public List<Sprite> allIconStatus;//thu tu: slp/psn/brn/par/frz
    public Image enemyStatusImg;
    public Image playerStatusImg;

    public void ShowMoveText(string t1, string t2, string t3, string t4)
    {
        move1Text.text = t1;
        move2Text.text = t2;
        move3Text.text = t3;
        move4Text.text = t4;
    }

    public void UpdateEnemyInfor(PkmInBattle pkmInBattle)
    {
        enemyName.text = pkmInBattle.PkmInfor.name.ToUpper();
        enemyLevel.text = "LV. "+pkmInBattle.PkmInfor.level;
    }

    public void UpdatePlayerInfor(PkmInBattle pkmInBattle)
    {
        playerName.text = pkmInBattle.PkmInfor.name.ToUpper();
        playerLevel.text = "LV. " + pkmInBattle.PkmInfor.level;   
    }

    public void UpdatePlayerHp(int currHp, int sumHp)
    {
        float tmp = (float)(currHp*1f / sumHp*1f);
        if (tmp < 0) tmp = 0;
        if (currHp < 0) currHp = 0;
        if (currHp > sumHp) currHp = sumHp;

        playerHealthBar.transform.DOScale(new Vector3(tmp, 1, 1), 0.5f).SetEase(Ease.OutCubic);
        if (currHp<= sumHp/3)
        {
            playerHealthBar.GetComponent<Image>().color = new Color32(248,88, 40, 255);
        }    
        else if(currHp<=sumHp/2)
        {
            playerHealthBar.GetComponent<Image>().color = new Color32(248, 176, 0, 255);
        }
        else
        {
            playerHealthBar.GetComponent<Image>().color = new Color32(24, 192, 32, 255);
        }
        playerHp.text = currHp+ "/" + sumHp;
    }

    public void UpdateEnemyHp(int currHp, int sumHp)
    {
        float tmp = (float)(currHp * 1f / sumHp * 1f);
        if (tmp < 0) tmp = 0;
        if (currHp < 0) currHp = 0;
        if (currHp >sumHp) currHp = sumHp;
        enemyHealthBar.transform.DOScale(new Vector3(tmp, 1, 1), 0.5f).SetEase(Ease.OutCubic);
        if (currHp <= sumHp / 3)
        {
            enemyHealthBar.GetComponent<Image>().color = new Color32(248, 88, 40, 255);
        }
        else if (currHp <= sumHp / 2)
        {
            enemyHealthBar.GetComponent<Image>().color = new Color32(248, 176, 0, 255);
        }
        else
        {
            enemyHealthBar.GetComponent<Image>().color = new Color32(24, 192, 32, 255);
        }
        
    }

    #region Notifycation
    public void ShowNoty(string message)
    {
        notiObj.SetActive(true);
        notiText.text = message;
    }

    public void CloseNotify()
    {
        notiText.text = "";
        notiObj.SetActive(false);
    }
    #endregion

    #region Status Pkm
    Sprite GetSprite(StatusPkmEnum statusPkm)
    {
        if (statusPkm == StatusPkmEnum.Normal) return allIconStatus[5];
        else if (statusPkm == StatusPkmEnum.Sleep) return allIconStatus[0];
        else if (statusPkm == StatusPkmEnum.Posion) return allIconStatus[1];
        else if (statusPkm == StatusPkmEnum.Burn) return allIconStatus[2];
        else if (statusPkm == StatusPkmEnum.Paralysis) return allIconStatus[3];
        else if (statusPkm == StatusPkmEnum.Freeze) return allIconStatus[4];
        return allIconStatus[5];
    }
    public void UpdateStatusPlayer(StatusPkmEnum statusPkm)
    {
        playerStatusImg.sprite = GetSprite(statusPkm);
    }
    public void UpdateStatusEnemy(StatusPkmEnum statusPkm)
    {
        enemyStatusImg.sprite = GetSprite(statusPkm);
    }
    #endregion
}
