﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculationFormulaBattleManager : Singleton<CalculationFormulaBattleManager>
{
    public int CountHP(PkmInfor pkmInfor)
    {
        int hp = 0;
        hp = Mathf.RoundToInt(((2 * pkmInfor.statsPkm.hp + pkmInfor.IVs + pkmInfor.EVsInfor.GetSumEV() / 4 + 100) * pkmInfor.level) / 100 + 10);
        return hp;
    }

    #region Caculation Damage Pkm
    public string notyText;
    /// <summary>
    /// critical tam tinh la 1.5f
    /// 
    /// </summary>
    /// <returns></returns>
    public int GetDamage(MoveInfor move, PkmInfor pkmAttack, PkmInfor pkmDefense, PkmInforInBattle pkmAttackinforInBattle, BattleManager battleManager)
    {
        int dmg = 0;
        notyText = "";

        float tmp = (((2 * pkmAttack.level) / 5f) + 2)*move.power;
        float modifer = Modifier(move, pkmAttack, pkmDefense, pkmAttackinforInBattle, battleManager);

        notyText = "modifer: " + modifer;

        if (move.category== CategoryMoveEnum.Physical)
        {
            dmg =Mathf.RoundToInt( (((tmp * pkmAttack.statsPkm.atk / pkmAttack.statsPkm.def) / 50f) + 2) * modifer );
            return dmg;
        }   
        else if(move.category == CategoryMoveEnum.Special)
        {
            dmg = Mathf.RoundToInt((((tmp * pkmAttack.statsPkm.spAtk / pkmAttack.statsPkm.spDef) / 50f) + 2) * modifer);
            return dmg;
        }    
        return dmg;
    }

    float Weather(MoveInfor move, BattleManager battle)
    {
        float wt = 1f;

        switch (battle.Weather)
        {
            case WeatherEnum.Clear:
                break;
            case WeatherEnum.Cloudy:
                break;
            case WeatherEnum.Rain:
                if (move.typeMove == TypePokemonEnum.Water)
                {
                    return 1.5f;
                }
                else if (move.typeMove == TypePokemonEnum.Fire)
                {
                    return 0.5f;
                }
                break;
            case WeatherEnum.Thunderstorm:
                break;
            case WeatherEnum.Snow:
                break;
            case WeatherEnum.Blizzard:
                break;
            case WeatherEnum.HarshSunlight:
                if (move.typeMove == TypePokemonEnum.Fire)
                {
                    return 1.5f;
                }
                else if (move.typeMove == TypePokemonEnum.Water)
                {
                    return 0.5f;
                }
                break;
            case WeatherEnum.Sandstorm:
                break;
            case WeatherEnum.Fog:
                break;
        }
        return wt;
    }

    /// <summary>
    /// sức mạnh cộng thêm khi ra chiêu cùng hệ
    /// </summary>
    /// <param name="move"></param>
    /// <param name="pkmInfor"></param>
    /// <returns></returns>
    float STAB(MoveInfor move, PkmInfor pkmInfor)
    {
        if (pkmInfor.abilities.Contains("ADAPTABILITY")) return 2;

        foreach (var a in pkmInfor.typePkm.TypePokemon)
        {
            if (move.typeMove == a) return 1.5f;
        }
        return 1f;
    }

    /// <summary>
    /// tuong khac he
    /// </summary>
    /// <param name="typeMv"></param>
    /// <param name="pkmGetDmg"></param>
    /// <returns></returns>
    float TypePkm(TypePokemonEnum typeMv, PkmInfor pkmGetDmg)
    {
        float kq = 1f;
        //return AllTypesPkm_DT.Instance.GetTypeInBattle(move.typeMove,pkm)
        foreach(var a in pkmGetDmg.typePkm.TypePokemon)
        {
            kq *= AllTypesPkm_DT.Instance.GetTypeInBattle(typeMv, a);
        }
        return kq;
    }

    float Burn(MoveInfor move, PkmInfor pkmInfor, PkmInforInBattle pkmInBattle)
    {
        if(pkmInBattle.StatusPkm == StatusPkmEnum.Burn && pkmInfor.abilities.Contains("GUTS")==false && move.category == CategoryMoveEnum.Physical)
        {
            return 0.5f;
        }
        return 1f;
    }

    float Modifier(MoveInfor move, PkmInfor pkmAttack, PkmInfor pkmDefense, PkmInforInBattle pkmAttackinforInBattle, BattleManager battleManager)
    {
        float md = 1;
        md = Weather(move, battleManager)*Random.RandomRange(0.85f,1f) * STAB(move, pkmAttack) * TypePkm(move.typeMove, pkmDefense) * Burn(move, pkmAttack, pkmAttackinforInBattle);
        return md;
    }    

    #endregion
}
