﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadSpritePkmMng : Singleton<LoadSpritePkmMng>
{
    private Dictionary<string, Sprite> spritesPkm = new Dictionary<string, Sprite>();

    public Dictionary<string, Sprite> SpritesPkm { get => spritesPkm; set => spritesPkm = value; }

    public Sprite GetSpriteByID(string id)
    {
        Sprite sp;
        if(spritesPkm.ContainsKey(id))
        {
            spritesPkm.TryGetValue(id, out sp);
            return sp;
        }
        else
        {
            sp= Resources.Load<Sprite>("PKM/" + id);
            spritesPkm.Add(id, sp);
            return sp;
        }
    }
}
