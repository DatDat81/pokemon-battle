﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NaturePkmEnum
{
    Adamant=0,
    Bashful=1,
    Bold=2,
    Brave=3,
    Calm=4,
    Careful=5,
    Docile=6,
    Gentle=7,
    Hardy=8,
    Hasty=9,
    Impish=10,
    Jolly=11,
    Lax=12,
    Lonely=13,
    Mild=14,
    Modest=15,
    Naive=16,
    Naughty=17,
    Quiet=18,
    Quirky=19,
    Rash=20,
    Relaxed=21,
    Sassy=22,
    Serious=23,
    Timid=24
}

public enum NatureChangeStats
{
    None = 0,
    Attack = 1,
    Defense = 2,
    Speed = 3,
    SpAttack = 4,
    SpDefense = 5
}