﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeatherEnum
{
    Clear=0,
    Cloudy=1,
    Rain=2,
    Thunderstorm=3,
    Snow=4,
    Blizzard=5,
    HarshSunlight=6,
    Sandstorm=7,
    Fog=8,
}
