﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StatusPkmEnum 
{
    Normal=0, 
    Posion=1,
    Burn=2,
    Paralysis=3,
    Freeze=4,
    Sleep=5,
    CanNotEscape=6,
    Confusion=7,
    Infatuation=8,
    LeechSeed=9

}
