﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PkmInfor
{
    public int id;
    public string name;
    public int level;
    public TypePkm typePkm;
    public BaseStatsPkm statsPkm;
    public List<string> abilities;
    public bool canUseHidenAbility = false;
    public NaturePkmEnum naturePkm; // ban tinh pkm
    public int IVs;// chi so ca the, nam trong khoang (0->31)
    public EVsInfor EVsInfor;// chi so no luc, 
    public MoveInfor[] moveInfors = new MoveInfor[4];

    public void SetUpData(PkmInfor pkmInfor)
    {
        if (pkmInfor == null) Debug.LogError("PkmInfor is Null!!!!");
        id = pkmInfor.id;
        name = pkmInfor.name;
        typePkm.TypePokemon = pkmInfor.typePkm.TypePokemon;
        statsPkm.SetUpData(pkmInfor.statsPkm);
        level = pkmInfor.level;
        naturePkm = pkmInfor.naturePkm;
        IVs = pkmInfor.IVs;
        EVsInfor.SetUpData(pkmInfor.EVsInfor);
        for(int i=0;i<4;i++)
        {
            if (pkmInfor.moveInfors[i] == null) continue;
            moveInfors[i] = new MoveInfor();
            moveInfors[i].SetUpData(pkmInfor.moveInfors[i]);
        }
    }

    public void SetUpAbilities(List<string> st)
    {
        for(int i=0;i<st.Count;i++)
        {
            abilities.Add(st[i]);
        }
    }
}
