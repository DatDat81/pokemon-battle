﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PkmInforInBattle 
{
    public int HP;
    /// <summary>
    /// check co the bi tac dong boi hieu ung phu nhu: Posion, Burn,..
    /// </summary>
    public bool beAffectedBySideEffects = true;

    [SerializeField]
    private StatusPkmEnum statusPkm = StatusPkmEnum.Normal;

    private bool canReduceAccuracy = true;
    private bool canReduceDef = true;



    public StatusPkmEnum StatusPkm { get => statusPkm; set => statusPkm = value; }
    public bool CanReduceAccuracy { get => canReduceAccuracy; set => canReduceAccuracy = value; }
    public bool CanReduceDef { get => canReduceDef; set => canReduceDef = value; }

    public void SetUpData(PkmInforInBattle pkm)
    {
        HP = pkm.HP;
    }
}
