﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PkmInBattle : MonoBehaviour
{
    [SerializeField]
    private PkmInfor pkmInfor;

    [SerializeField]
    private PkmInforInBattle pkmInforInBattle;
    public PkmInfor PkmInfor { get => pkmInfor; set => pkmInfor = value; }
    public PkmInforInBattle PkmInforInBattle { get => pkmInforInBattle; set => pkmInforInBattle = value; }

    public void SetUpData(PkmInBattle pkm)
    {
        if (pkm == null) Debug.LogError("PkmInBattle is Null !!!");
        pkmInfor = new PkmInfor();
        pkmInforInBattle = new PkmInforInBattle();
        pkmInfor.SetUpData(pkm.pkmInfor);
        pkmInforInBattle.SetUpData(pkm.pkmInforInBattle);
    }
}
