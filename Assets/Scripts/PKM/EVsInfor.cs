﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// chỉ số ẩn tác động đến sức mạnh của Pokémon trong vài chỉ số nhất định. 
/// Sau khi đánh thắng 1 Pokémon thì Pokémon địch sẽ cho bạn điểm EV. 
/// Mỗi loại Pokémon sẽ cho điểm EV nhất định của một chỉ số nhất định. 
/// Khi Pokémon có 4 EV point chỉ số nào thì sẽ cộng 1 điểm vào chỉ số tương ứng
/// </summary>
[System.Serializable]
public class EVsInfor
{
    public int ev_hp;
    public int ev_atk;
    public int ev_def;
    public int ev_speed;
    public int ev_spAtk;    
    public int ev_spDef;


    public void SetUpData(EVsInfor eVs)
    {
        ev_hp = eVs.ev_hp;
        ev_atk = eVs.ev_atk;
        ev_def = eVs.ev_def;
        ev_speed = eVs.ev_speed;
        ev_spAtk = eVs.ev_spAtk;
        ev_spDef = eVs.ev_spDef;
    }

    public int GetSumEV()
    {
        return ev_atk + ev_def + ev_hp + ev_spAtk + ev_spDef + ev_speed;
    }
}

