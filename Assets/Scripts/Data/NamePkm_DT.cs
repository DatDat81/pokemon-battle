﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NamePkm_DT : Singleton<NamePkm_DT>
{
    [SerializeField]
    private List<string> _namePkm;



    public List<string> NamePkm { get => _namePkm; set => _namePkm = value; }

    private void Start()
    {
        if (_namePkm == null) _namePkm = new List<string>();
    }

}
