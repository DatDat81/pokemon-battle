﻿using System.Collections.Generic;

public class AllTypesPkm_DT : Singleton<AllTypesPkm_DT>
{
    // name Type _ list
    private Dictionary<string, EngravedTypePkm> allTypePkms = new Dictionary<string, EngravedTypePkm>();

    public Dictionary<string, EngravedTypePkm> AllTypePkms { get => allTypePkms; set => allTypePkms = value; }

    /// <summary>
    /// Get Type Pkm Defenses
    /// </summary>
    /// <param name="typePkmDefense"></param>
    /// <returns></returns>
    public EngravedTypePkm GetEngravedPkmByType(string typePkmDefense)
    {
        EngravedTypePkm engravedTypePkm = new EngravedTypePkm();
        allTypePkms.TryGetValue(typePkmDefense,out engravedTypePkm);
        return engravedTypePkm;
    }

    /// <summary>
    /// tra ve he so tuong khac cua chieu thuc pkm tan cong 
    /// va pkm phong thu
    /// </summary>
    /// <param name="attackerType"></param>
    /// <param name="defenseType"></param>
    /// <returns></returns>
    public float GetTypeInBattle(TypePokemonEnum attackerType, TypePokemonEnum defenseType)
    {
        EngravedTypePkm engravedType= GetEngravedPkmByType(defenseType.ToString());
        if(engravedType.weaknesses.Contains(attackerType))
        {
            return 2;
        }   
        else if(engravedType.immunities.Contains(attackerType))
        {
            return 0;
        }   
        else if(engravedType.resistances.Contains(attackerType))
        {
            return 0.5f;
        }
        return 1;
    }    
}
