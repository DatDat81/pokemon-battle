﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilitiesPkm_DT : Singleton<AbilitiesPkm_DT>
{
    [SerializeField]
    private Dictionary<string, AbilitiesPkm> abilities = new Dictionary<string, AbilitiesPkm>();

    public Dictionary<string, AbilitiesPkm> Abilities { get => abilities; set => abilities = value; }

    public AbilitiesPkm GetAbilitiesPkm(string pkmName)
    {
        AbilitiesPkm smt;
        Abilities.TryGetValue(pkmName, out smt);
        return smt;
    }    
}

    
