﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HidenAbilitiesPkm 
{
    private string hidenAbility;

    public string HidenAbility { get => hidenAbility; set => hidenAbility = value; }
}
