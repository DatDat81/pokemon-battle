﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// cho biet he nay Weaknesses_Resistances_Immunities voi nhung he nao 
/// </summary>
[System.Serializable]
public class EngravedTypePkm 
{
    public TypePokemonEnum type;
    public bool isSpecialType = false;
    public List<TypePokemonEnum> weaknesses = new List<TypePokemonEnum>();
    public List<TypePokemonEnum> resistances = new List<TypePokemonEnum>();
    public List<TypePokemonEnum> immunities = new List<TypePokemonEnum>();
}
