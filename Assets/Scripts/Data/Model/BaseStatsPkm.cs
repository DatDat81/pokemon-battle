﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseStatsPkm 
{
    public int hp;
    public int atk;
    public int def;
    public int speed;
    public int spAtk;
    public int spDef;

    public void SetUpData(BaseStatsPkm statsPkm)
    {
        hp = statsPkm.hp;
        atk = statsPkm.atk;
        def = statsPkm.def;
        speed = statsPkm.speed;
        spAtk = statsPkm.spAtk;
        spDef = statsPkm.spDef;
    }
}
