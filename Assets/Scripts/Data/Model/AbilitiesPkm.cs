﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AbilitiesPkm 
{
    private List<string> abilities = new List<string>();

    public List<string> Abilities { get => abilities; set => abilities = value; }
}
