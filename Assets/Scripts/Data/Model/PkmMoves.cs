﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// content list move of a pkm
/// </summary>
public class PkmMovesList
{
    private List<PkmMoves> pkmMoves = new List<PkmMoves>();

    public List<PkmMoves> PkmMoves { get => pkmMoves; set => pkmMoves = value; }
}

public class PkmMoves
{
    public int levelCanLearn;
    public string moveName;
}
