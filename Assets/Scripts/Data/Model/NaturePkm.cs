﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Bản tính sẽ giúp tăng một hoặc giảm một chỉ số nhất định đi 10%.
/// </summary>
[System.Serializable]
public class NaturePkm 
{
    public NatureChangeStats upStats; // up 10%
    public NatureChangeStats downStats;// down 10%
}
