﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TypePkm
{
    [SerializeField]
    private List<TypePokemonEnum> typePokemon; //// mot pkm co the co nhieu type

    public List<TypePokemonEnum> TypePokemon { get => typePokemon; set => typePokemon = value; }
}
