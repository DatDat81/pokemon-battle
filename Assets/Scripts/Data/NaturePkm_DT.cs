﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaturePkm_DT : Singleton<NaturePkm_DT>
{
    private Dictionary<string, NaturePkm> naturePkms;

    public Dictionary<string, NaturePkm> NaturePkms { get => naturePkms; set => naturePkms = value; }

    private void Start()
    {
        if (naturePkms == null) naturePkms = new Dictionary<string, NaturePkm>();
    }

    public void DoNatureUp(ref PkmInfor pkmInfor, NatureChangeStats natureChangeStats)
    {
        int tmp=0;
        switch (natureChangeStats)
        {
            
            case NatureChangeStats.Attack:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.atk + 0.1f * pkmInfor.statsPkm.atk);
                pkmInfor.statsPkm.atk = tmp;
                break;
            case NatureChangeStats.Defense:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.def + 0.1f * pkmInfor.statsPkm.def);
                pkmInfor.statsPkm.def = tmp;
                break;
            case NatureChangeStats.Speed:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.speed + 0.1f * pkmInfor.statsPkm.speed);
                pkmInfor.statsPkm.speed = tmp;
                break;
            case NatureChangeStats.SpAttack:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.spAtk + 0.1f * pkmInfor.statsPkm.spAtk);
                pkmInfor.statsPkm.spAtk = tmp;
                break;
            case NatureChangeStats.SpDefense:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.spDef + 0.1f * pkmInfor.statsPkm.spDef);
                pkmInfor.statsPkm.spDef = tmp;
                break;
        }
    }

    public void DoNatureDown(ref PkmInfor pkmInfor, NatureChangeStats natureChangeStats)
    {
        int tmp = 0;
        switch (natureChangeStats)
        {

            case NatureChangeStats.Attack:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.atk - 0.1f * pkmInfor.statsPkm.atk);
                pkmInfor.statsPkm.atk = tmp;
                break;
            case NatureChangeStats.Defense:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.def - 0.1f * pkmInfor.statsPkm.def);
                pkmInfor.statsPkm.def = tmp;
                break;
            case NatureChangeStats.Speed:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.speed - 0.1f * pkmInfor.statsPkm.speed);
                pkmInfor.statsPkm.speed = tmp;
                break;
            case NatureChangeStats.SpAttack:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.spAtk - 0.1f * pkmInfor.statsPkm.spAtk);
                pkmInfor.statsPkm.spAtk = tmp;
                break;
            case NatureChangeStats.SpDefense:
                tmp = Mathf.RoundToInt(pkmInfor.statsPkm.spDef - 0.1f * pkmInfor.statsPkm.spDef);
                pkmInfor.statsPkm.spDef = tmp;
                break;
        }
    }
}
