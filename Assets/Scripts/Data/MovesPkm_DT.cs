﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovesPkm_DT : Singleton<MovesPkm_DT>
{

    /// <summary>
    /// key: Name Pkm
    /// value: Move List
    /// </summary>
    private Dictionary<string, PkmMovesList> movesPkm;

    public Dictionary<string, PkmMovesList> MovesPkm { get => movesPkm; set => movesPkm = value; }

    private void Start()
    {
        if (movesPkm == null) movesPkm = new Dictionary<string, PkmMovesList>();
    }

    public PkmMovesList GetListMoveOfPkm(string pkmName)
    {
        PkmMovesList lst = new PkmMovesList();
        MovesPkm.TryGetValue(pkmName, out lst);
        return lst;
    }

    public string GetNameMoveBaseOnLevelCanLearn(string namePkm, int levelCanLearn)
    {
        string namemv = "";
        PkmMovesList pkmMovesList = GetListMoveOfPkm(namePkm);
        if (pkmMovesList == null) return "";
        foreach(var pk in pkmMovesList.PkmMoves)
        {
            if (levelCanLearn == pk.levelCanLearn)
                return pk.moveName;
        }
        return namemv;
    }

    public List<string> GetListMoveCanLearnBaseOnCurrenLevel(string namePkm, int currentLv)
    {
        List<string> lstMove = new List<string>();

        for(int i=1;i<=currentLv;i++)
        {
            string s = GetNameMoveBaseOnLevelCanLearn(namePkm, i);
            if(s!="")
            {
                lstMove.Add(s);
            }
        }
        return lstMove;
    }

      
}
