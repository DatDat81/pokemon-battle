﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidenAbilitiesPkm_DT : Singleton<HidenAbilitiesPkm_DT>
{
    // namePkm_hidenAbilities
    private Dictionary<string, HidenAbilitiesPkm> hidenAbilitiesPkms = new Dictionary<string, HidenAbilitiesPkm>();

    public Dictionary<string, HidenAbilitiesPkm> HidenAbilitiesPkms { get => hidenAbilitiesPkms; set => hidenAbilitiesPkms = value; }

    public HidenAbilitiesPkm GetHidenAbilitiesPkm(string namePkm)
    {
        
        HidenAbilitiesPkm hide;
        HidenAbilitiesPkms.TryGetValue(namePkm, out hide);
        return hide;
    }    
}
