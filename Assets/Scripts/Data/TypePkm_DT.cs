﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypePkm_DT : Singleton<TypePkm_DT>
{
    [SerializeField]
    private List<TypePkm> typePkms;

    public List<TypePkm> TypePkms { get => typePkms; set => typePkms = value; }

    void Start()
    {
        if (typePkms == null) typePkms = new List<TypePkm>();
    }

    public TypePokemonEnum ConvertTypePkmToEnum(string type)
    {
        TypePokemonEnum typePkm = TypePokemonEnum.Normal;
        switch (type)
        {

            case "NORMAL":
                typePkm = TypePokemonEnum.Normal;
                break;
            case "FIRE":
                typePkm = TypePokemonEnum.Fire;
                break;
            case "WATER":
                typePkm = TypePokemonEnum.Water;
                break;
            case "GRASS":
                typePkm = TypePokemonEnum.Grass;
                break;
            case "ELECTRIC":
                typePkm = TypePokemonEnum.Electric;
                break;
            case "ICE":
                typePkm = TypePokemonEnum.Ice;
                break;
            case "FIGHTING":
                typePkm = TypePokemonEnum.Fighting;
                break;
            case "POISON":
                typePkm = TypePokemonEnum.Poison;
                break;
            case "GROUND":
                typePkm = TypePokemonEnum.Ground;
                break;
            case "FLYING":
                typePkm = TypePokemonEnum.Flying;
                break;
            case "PSYCHIC":
                typePkm = TypePokemonEnum.Psychic;
                break;
            case "BUG":
                typePkm = TypePokemonEnum.Bug;
                break;
            case "ROCK":
                typePkm = TypePokemonEnum.Rock;
                break;
            case "GHOST":
                typePkm = TypePokemonEnum.Ghost;
                break;
            case "DRAGON":
                typePkm = TypePokemonEnum.Dragon;
                break;
            case "DARK":
                typePkm = TypePokemonEnum.Dark;
                break;
            case "STEEL":
                typePkm = TypePokemonEnum.Steel;
                break;
            case "FAIRY":
                typePkm = TypePokemonEnum.Fairy;
                break;
            default:
                Debug.LogError("A Type Is Out: " + type);
                break;
        }
        return typePkm;
    }
}
