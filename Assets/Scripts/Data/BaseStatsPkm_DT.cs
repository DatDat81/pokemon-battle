﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseStatsPkm_DT : Singleton<BaseStatsPkm_DT>
{
    [SerializeField]
    List<BaseStatsPkm> baseStatsPkms;
    public List<BaseStatsPkm> BaseStatsPkms { get => baseStatsPkms; set => baseStatsPkms = value; }

    private void Start()
    {
        baseStatsPkms = new List<BaseStatsPkm>();
    }
}
