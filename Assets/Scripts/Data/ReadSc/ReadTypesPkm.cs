﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// doc cac he tuong khac pokemon dung trong cong thuc tinh damage
/// </summary>
public class ReadTypesPkm : MonoBehaviour
{
    public TextAsset typeDt;

    private void Start()
    {
        string[] dt = typeDt.ToString().Split('\n');
        EngravedTypePkm engravedType = new EngravedTypePkm();
        string name = "";
        for (int i = 0; i < dt.Length; i++)
        {
            if (dt[i].StartsWith("#"))
            {
                AllTypesPkm_DT.Instance.AllTypePkms.Add(name, engravedType);
                name = "";
                engravedType = new EngravedTypePkm();
            }
            else if (dt[i].StartsWith("Name"))
            {
                string[] s = dt[i].Split('=');
                name = s[1].TrimEnd();
            }
            else if (dt[i].StartsWith("IsSpecialType"))
            {
                engravedType.isSpecialType = true;
            }
            else if (dt[i].StartsWith("Weaknesses"))
            {
                string[] s = dt[i].Split('=');
                string[] s2 = s[1].Split(',');

                for (int k = 0; k < s2.Length; k++)
                {
                    TypePokemonEnum tp;
                    string t = s2[k].TrimEnd();
                    string t1 = t.Substring(1, t.Length - 1);
                    string t2 = t.Substring(0, 1);
                    string st = t2 + t1.ToLower();
                    Enum.TryParse<TypePokemonEnum>(st, out tp);
                    engravedType.weaknesses.Add(tp);
                }
            }
            else if (dt[i].StartsWith("Resistances"))
            {
                string[] s = dt[i].Split('=');
                string[] s2 = s[1].Split(',');

                for (int k = 0; k < s2.Length; k++)
                {
                    TypePokemonEnum tp;
                    string t = s2[k].TrimEnd();
                    string t1 = t.Substring(1, t.Length - 1);
                    string t2 = t.Substring(0, 1);
                    string st = t2 + t1.ToLower();
                    Enum.TryParse<TypePokemonEnum>(st, out tp);
                    engravedType.resistances.Add(tp);
                }
            }
            else if (dt[i].StartsWith("Immunities"))
            {
                string[] s = dt[i].Split('=');
                string[] s2 = s[1].Split(',');

                for (int k = 0; k < s2.Length; k++)
                {
                    TypePokemonEnum tp;
                    string t = s2[k].TrimEnd();
                    string t1 = t.Substring(1, t.Length - 1);
                    string t2 = t.Substring(0, 1);
                    string st = t2 + t1.ToLower();
                    Enum.TryParse<TypePokemonEnum>(st, out tp);
                    engravedType.immunities.Add(tp);
                }
            }
        }
    }

    //private void Update()
    //{
    //    lock(this.components)
    //    {

    //    }    
    //}
}
