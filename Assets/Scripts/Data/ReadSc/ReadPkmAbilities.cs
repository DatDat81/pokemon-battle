﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadPkmAbilities : MonoBehaviour
{
    public TextAsset abilitiesDiscription;

    private void Start()
    {
        string[] dt = abilitiesDiscription.ToString().Split('\n');
        for(int i=0;i<dt.Length;i++)
        {
            string[] s1 = dt[i].Split(',');
            string[] s2 = dt[i].Split('"');
            AbilitiesDiscription.Instance.AblitiesDisctiption.Add(s1[1].TrimEnd(), s2[1].TrimEnd());
        }
    }
}
