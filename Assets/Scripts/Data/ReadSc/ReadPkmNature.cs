﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadPkmNature : MonoBehaviour
{
    public TextAsset natureAsset;

    private void Start()
    {
        string dt = natureAsset.ToString();

        string[] split = dt.Split('\n');

        for (int i = 0; i < split.Length; i++)
        {
            NaturePkm np = new NaturePkm();

            string[] s = split[i].Split('=');
            string[] n = s[1].Split(',');

            if (n[0].TrimEnd() != "")
            {
                np.upStats = ConvertStringToStatsPkm(n[0].TrimEnd());
            }

            if (n[1].TrimEnd() != "")
            {
                np.downStats = ConvertStringToStatsPkm(n[1].TrimEnd());
            }
            NaturePkm_DT.Instance.NaturePkms.Add(s[0], np);
        }
        //NaturePkm test = new NaturePkm();
        //NaturePkm_DT.Instance.NaturePkms.TryGetValue("Timid", out test);
        //Debug.Log(test.upStats + " " + test.downStats);
    }

    NatureChangeStats ConvertStringToStatsPkm(string s)
    {
        NatureChangeStats statsPkmEnum = NatureChangeStats.Attack;
        switch (s)
        {
            case "":
                statsPkmEnum = NatureChangeStats.None;
                break;
            case "Attack":
                statsPkmEnum = NatureChangeStats.Attack;
                break;
            case "Defense":
                statsPkmEnum = NatureChangeStats.Defense;
                break;
            case "Speed":
                statsPkmEnum = NatureChangeStats.Speed;
                break;
            case "SpAttack":
                statsPkmEnum = NatureChangeStats.SpAttack;
                break;
            case "SpDefense":
                statsPkmEnum = NatureChangeStats.SpDefense;
                break;
            default:
                Debug.LogError("Stats Convert Is Out!!: "+s);
                break;
        }
        return statsPkmEnum;
    }
}
