﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadPKMData : MonoBehaviour
{
    public TextAsset pkmDT;
    void Start()
    {
        ReadData(pkmDT.ToString());
    }

    int countPkm = 0;
    void ReadData(string dt)
    {
        string[] textBox = dt.Split('\n');
        TypePkm typesPkm = new TypePkm();
        typesPkm.TypePokemon = new List<TypePokemonEnum>();
        

        for (int i = 0; i < textBox.Length; i++)
        {
            if(textBox[i].StartsWith("#") && i!=0)
            {
                if(typesPkm != null)
                {
                    if(typesPkm.TypePokemon!=null)
                    {
                        TypePkm_DT.Instance.TypePkms.Add(typesPkm);
                    }   
                }
                typesPkm = new TypePkm();
                typesPkm.TypePokemon = new List<TypePokemonEnum>();
                countPkm += 1;
            }
            
            if (textBox[i].StartsWith("Name"))
            {
                string[] nameCut = textBox[i].Split('=');
                NamePkm_DT.Instance.NamePkm.Add(nameCut[1].TrimEnd()); // trim to cut '/0' in last of string
            }    
            else if(textBox[i].StartsWith("Type"))
            {
                string[] s = textBox[i].Split('=');
                typesPkm.TypePokemon.Add(TypePkm_DT.Instance.ConvertTypePkmToEnum(s[1].TrimEnd()));
            }
            else if (textBox[i].StartsWith("BaseStats"))
            {
                HandleBaseStart(textBox[i]);
            }
            else if(textBox[i].StartsWith("Abilities"))
            {
                AbilitiesPkm abilitiesPkm = new AbilitiesPkm();
                string[] s = textBox[i].Split('=');
                string[] ab = s[1].Split(',');
                for(int k=0;k<ab.Length;k++)
                {
                    abilitiesPkm.Abilities.Add(ab[k].TrimEnd());
                }
                AbilitiesPkm_DT.Instance.Abilities.Add(NamePkm_DT.Instance.NamePkm[countPkm], abilitiesPkm);
            }
            else if (textBox[i].StartsWith("HiddenAbility"))
            {
                HidenAbilitiesPkm hidenAbilitiesPkm = new HidenAbilitiesPkm();
                string[] s = textBox[i].Split('=');
                string[] ab = s[1].Split(',');
                hidenAbilitiesPkm.HidenAbility = ab[0].TrimEnd();
                HidenAbilitiesPkm_DT.Instance.HidenAbilitiesPkms.Add(NamePkm_DT.Instance.NamePkm[countPkm], hidenAbilitiesPkm);
            }
            else if(textBox[i].StartsWith("Moves"))
            {
                string[] s = textBox[i].Split('=');
                string[] mv = s[1].Split(',');
                PkmMovesList pkmMovesList = new PkmMovesList();
                for (int j = 0; j < mv.Length; j += 2)
                {
                    PkmMoves pkmMoves = new PkmMoves();
                    int.TryParse(mv[j].TrimEnd(), out pkmMoves.levelCanLearn);
                    pkmMoves.moveName = mv[j + 1].TrimEnd();
                    pkmMovesList.PkmMoves.Add(pkmMoves);
                }
                MovesPkm_DT.Instance.MovesPkm.Add(NamePkm_DT.Instance.NamePkm[countPkm], pkmMovesList);
            }
        }
    }    

    void HandleBaseStart(string st)
    {
        string[] s = st.Split('=');
        string[] s2 = s[1].Split(',');
        BaseStatsPkm baseStatsPkm = new BaseStatsPkm();
        int[] a = new int[6];
        for (int j = 0; j < s2.Length; j++)
        {
            int.TryParse(s2[j], out a[j]);
        }

        baseStatsPkm.hp = a[0];
        baseStatsPkm.atk = a[1];
        baseStatsPkm.def = a[2];
        baseStatsPkm.speed = a[3];
        baseStatsPkm.spAtk = a[4];
        baseStatsPkm.spDef = a[5];
        BaseStatsPkm_DT.Instance.BaseStatsPkms.Add(baseStatsPkm);
    }
}
