﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class MoveButtonUI : MonoBehaviour,IPointerClickHandler
{
    public Text nameText;

    public void OnPointerClick(PointerEventData eventData)
    {
        if(BattleManager.Instance.battleThread == BattleThreadEnum.StartTurn)
        {
            BattleManager.Instance.PlayerMoveLastUsed = nameText.text;
            BattleManager.Instance.battleThread = BattleThreadEnum.BeforeAttack;
        }
    }
}
