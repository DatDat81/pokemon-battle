﻿
public enum BattleThreadEnum
{
    WaitStatus=0,
    StartBattle=1,
    StartTurn=2,
    BeforeAttack=3,
    InAttack=4,
    AfterAttack=5,
    EndTurn=6,
    ChangePkmInTurn=7,
    ChangePkmOutTurn=8,
    EndBattle=9
}
