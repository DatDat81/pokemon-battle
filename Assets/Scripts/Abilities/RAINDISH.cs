﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Nếu trời mưa, Pokémon này sẽ hồi phục 1/16 HP ở cuối lượt.
/// </summary>
public class RAINDISH : BaseAbility,IAbility
{
    bool addHp = false;
    public void OnStartBattle(BattleManager battleManager)
    {

    }

    public void OnStartTurn(BattleManager battleManager)
    {
        addHp = false;
    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
        if (battleManager.Weather == WeatherEnum.Rain && addHp ==false)
        {
            addHp = true;
            if (IsPlayer())
            {
                ShowMessage();
                int spUp = Mathf.RoundToInt(battleManager.DataMng.player.PkmInforInBattle.HP/16);
                if (spUp < 1) spUp = 1;
                battleManager.PlayerHpCurr += spUp;
            }
            else
            {
                int spUp = Mathf.RoundToInt(battleManager.DataMng.enemy.PkmInforInBattle.HP / 16);
                if (spUp < 1) spUp = 1;
                battleManager.EnemyHpCurr += spUp;
            }
        }
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
