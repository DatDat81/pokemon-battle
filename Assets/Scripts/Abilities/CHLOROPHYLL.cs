﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Speed của Pokémon này sẽ được nhân đôi trong lúc trời nắng.
/// </summary>
public class CHLOROPHYLL : BaseAbility,IAbility
{
    bool isSpeedUp = false;
    int speedUp = 0;
    public void OnStartBattle(BattleManager battleManager)
    {

    }

    public void OnStartTurn(BattleManager battleManager)
    {
        if(isSpeedUp==false)
        {
            if(battleManager.Weather == WeatherEnum.HarshSunlight)
            {
                isSpeedUp = true;
                if (IsPlayer())
                {
                    ShowMessage();
                    speedUp = battleManager.PlayerInforCurr.statsPkm.speed ;
                    battleManager.PlayerInforCurr.statsPkm.speed += speedUp;
                }
                else
                {
                    speedUp = battleManager.EnemyInforCurr.statsPkm.speed;
                    battleManager.EnemyInforCurr.statsPkm.speed += speedUp;
                }
            }
        }
        else
        {
            if (battleManager.Weather != WeatherEnum.HarshSunlight)
            {
                if (speedUp != 0)
                {
                    if (IsPlayer())
                    {
                        battleManager.PlayerInforCurr.statsPkm.speed -= speedUp;
                        speedUp = 0;
                    }
                    else
                    {
                        battleManager.EnemyInforCurr.statsPkm.speed -= speedUp;
                        speedUp = 0;
                    }
                }
                isSpeedUp = false;
            }
        }
    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }

    public void OnDie(BattleManager battleManager)
    {

    }

    public void OnEndBattle(BattleManager battleManager)
    {

    }

    public void OnChangePokemonOut(BattleManager battleManager)
    {

    }
}
