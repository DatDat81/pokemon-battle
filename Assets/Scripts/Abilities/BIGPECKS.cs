﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Đối phương không thể giảm Defense của Pokémon này, tuy nhiên Pokémon vẫn bị giảm bởi các chiêu thức thay đổi chỉ số trực tiếp như Power Swap, Guard Swap và Power Split. Đặc tính này không hề ngăn Pokémon tự giảm Defense.
/// </summary>
public class BIGPECKS : BaseAbility,IAbility
{
    public void OnStartBattle(BattleManager battleManager)
    {
        if (IsPlayer())
        {
            battleManager.DataMng.player.PkmInforInBattle.CanReduceDef = false;
        }
        else
        {
            battleManager.DataMng.enemy.PkmInforInBattle.CanReduceDef = false;
        }
    }

    public void OnStartTurn(BattleManager battleManager)
    {

    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
        if (IsPlayer())
        {
            battleManager.DataMng.player.PkmInforInBattle.CanReduceDef = false;
        }
        else
        {
            battleManager.DataMng.enemy.PkmInforInBattle.CanReduceDef = false;
        }
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
