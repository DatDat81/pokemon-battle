﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Khi Pokémon này còn đúng hay dưới 1/3 HP tối đa, Attack và Special Attack của nó sẽ tăng 50% nếu chiêu thức hệ Bọ được sử dụng khi nó ra đòn.
/// </summary>
public class SWARM : BaseAbility,IAbility
{
    int upAt = 0, upSpAt = 0;
    public void OnStartBattle(BattleManager battleManager)
    {

    }

    public void OnStartTurn(BattleManager battleManager)
    {

    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
        upAt = 0;
        upSpAt = 0;
        if (IsPlayer())
        {
            if (battleManager.PlayerHpCurr <= battleManager.DataMng.player.PkmInforInBattle.HP / 3)
            {
                if (BattleManager.Instance.PlayerMoveLastUsed == "") return;
                MoveInfor mv = AllListMoves.Instance.GetMoveInfor(BattleManager.Instance.PlayerMoveLastUsed);
                if (mv.typeMove == TypePokemonEnum.Bug)
                {
                    ShowMessage();
                    upAt = Mathf.RoundToInt(battleManager.PlayerInforCurr.statsPkm.atk * 0.5f);
                    upSpAt = Mathf.RoundToInt(battleManager.PlayerInforCurr.statsPkm.spAtk * 0.5f);
                    battleManager.PlayerInforCurr.statsPkm.atk += upAt;
                    battleManager.PlayerInforCurr.statsPkm.spAtk += upSpAt;
                }
            }
        }
        else
        {
            if (battleManager.EnemyHpCurr <= battleManager.DataMng.enemy.PkmInforInBattle.HP / 3)
            {
                if (BattleManager.Instance.PlayerMoveLastUsed == "") return;
                MoveInfor mv = AllListMoves.Instance.GetMoveInfor(BattleManager.Instance.PlayerMoveLastUsed);
                if (mv.typeMove == TypePokemonEnum.Bug)
                {
                    upAt = Mathf.RoundToInt(battleManager.EnemyInforCurr.statsPkm.atk * 0.5f);
                    upSpAt = Mathf.RoundToInt(battleManager.EnemyInforCurr.statsPkm.spAtk * 0.5f);
                    battleManager.EnemyInforCurr.statsPkm.atk += upAt;
                    battleManager.EnemyInforCurr.statsPkm.spAtk += upSpAt;
                }
            }
        }
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
        if (IsPlayer())
        {
            if (battleManager.PlayerHpCurr <= battleManager.DataMng.player.PkmInforInBattle.HP / 3)
            {
                battleManager.PlayerInforCurr.statsPkm.atk -= upAt;
                battleManager.PlayerInforCurr.statsPkm.spAtk -= upSpAt;

            }
        }
        else
        {
            if (battleManager.EnemyHpCurr <= battleManager.DataMng.enemy.PkmInforInBattle.HP / 3)
            {
                battleManager.EnemyInforCurr.statsPkm.atk -= upAt;
                battleManager.EnemyInforCurr.statsPkm.spAtk -= upSpAt;

            }
        }
        upAt = 0;
        upSpAt = 0;
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
