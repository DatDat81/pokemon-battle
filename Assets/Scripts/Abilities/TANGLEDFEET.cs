﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Nếu Pokémon này bị choáng váng, các chiêu thức được tung lên nó sẽ bị giảm 50% Accuracy.
/// </summary>
public class TANGLEDFEET : BaseAbility,IAbility
{
    public void OnStartBattle(BattleManager battleManager)
    {

    }

    public void OnStartTurn(BattleManager battleManager)
    {
        Debug.Log("Tang");
    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
        if(IsPlayer())
        {
            if(battleManager.DataMng.player.PkmInforInBattle.StatusPkm== StatusPkmEnum.Confusion)
            {
                ShowMessage();
                for (int i = 0; i < 4; i++)
                {
                    if (battleManager.PlayerInforCurr.moveInfors[i] == null) continue;
                    battleManager.PlayerInforCurr.moveInfors[i].accuracy -= Mathf.RoundToInt(battleManager.PlayerInforCurr.moveInfors[i].accuracy * 0.5f);
                }
            }
        }
        else
        {
            if (battleManager.DataMng.enemy.PkmInforInBattle.StatusPkm == StatusPkmEnum.Confusion)
            {
                ShowMessage();
                for (int i = 0; i < 4; i++)
                {
                    if (battleManager.EnemyInforCurr.moveInfors[i] == null) continue;
                    battleManager.EnemyInforCurr.moveInfors[i].accuracy -= Mathf.RoundToInt(battleManager.EnemyInforCurr.moveInfors[i].accuracy * 0.5f);
                }
            }
        }
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
        if (IsPlayer())
        {
            for (int i = 0; i < 4; i++)
            {
                if (battleManager.PlayerInforCurr.moveInfors[i] == null) continue;
                battleManager.PlayerInforCurr.moveInfors[i].accuracy = battleManager.DataMng.player.PkmInfor.moveInfors[i].accuracy;
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                if (battleManager.EnemyInforCurr.moveInfors[i] == null) continue;
                battleManager.EnemyInforCurr.moveInfors[i].accuracy = battleManager.DataMng.enemy.PkmInfor.moveInfors[i].accuracy;
            }
        }
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
