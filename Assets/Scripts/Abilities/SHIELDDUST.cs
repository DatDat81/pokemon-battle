﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Nếu đối phương sử dụng chiêu thức có hiệu ứng phụ để gây sát thương lên Pokémonn này thì hiệu ứng phụ của chiêu thức đó sẽ không được kích hoạt.
/// </summary>
public class SHIELDDUST : BaseAbility,IAbility
{
    public void OnStartBattle(BattleManager battleManager)
    {
        if(IsPlayer())
        {
            battleManager.DataMng.player.PkmInforInBattle.beAffectedBySideEffects = false;
        }
        else
        {
            battleManager.DataMng.enemy.PkmInforInBattle.beAffectedBySideEffects = false;
        }
    }

    public void OnStartTurn(BattleManager battleManager)
    {

    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
