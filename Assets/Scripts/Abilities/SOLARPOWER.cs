﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Nếu Pokémon này được tung ra khi trời nắng, Special Attack của nó sẽ tạm thời tăng 50% nhưng đồng thời nó cũng nhận sát thương tương ứng với 1/8 HP tối đa sau mỗi lượt.
/// </summary>
public class SOLARPOWER : BaseAbility,IAbility
{
    bool isChangeSpAtk = false;
    int spUp = 0;
    public void OnStartBattle(BattleManager battleManager)
    {
        if (isChangeSpAtk == false && battleManager.Weather == WeatherEnum.HarshSunlight)
        {
            spUp = 0;
            if (IsPlayer())
            {
                ShowMessage();
                spUp = Mathf.RoundToInt(battleManager.PlayerInforCurr.statsPkm.spAtk * 0.5f);
                battleManager.PlayerInforCurr.statsPkm.spAtk += spUp;
                isChangeSpAtk = true;
            }
            else
            {
                spUp = Mathf.RoundToInt(battleManager.EnemyInforCurr.statsPkm.spAtk * 0.5f);
                battleManager.EnemyInforCurr.statsPkm.spAtk += spUp;
                isChangeSpAtk = true;
            }
        }
    }

    public void OnStartTurn(BattleManager battleManager)
    {
        
    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
        if(isChangeSpAtk)
        {
            if (IsPlayer())
            {
                battleManager.PlayerHpCurr -= Mathf.RoundToInt(battleManager.DataMng.player.PkmInforInBattle.HP /8f);
            }
            else
            {
                battleManager.EnemyHpCurr -= Mathf.RoundToInt(battleManager.DataMng.enemy.PkmInforInBattle.HP /8f);
            }
        }
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {
        if (isChangeSpAtk == false && battleManager.Weather == WeatherEnum.HarshSunlight)
        {
            spUp = 0;
            if (IsPlayer())
            {
                ShowMessage();
                spUp = Mathf.RoundToInt(battleManager.PlayerInforCurr.statsPkm.spAtk * 0.5f);
                battleManager.PlayerInforCurr.statsPkm.spAtk += spUp;
                isChangeSpAtk = true;
            }
            else
            {
                spUp = Mathf.RoundToInt(battleManager.EnemyInforCurr.statsPkm.spAtk * 0.5f);
                battleManager.EnemyInforCurr.statsPkm.spAtk += spUp;
                isChangeSpAtk = true;
            }
        }
    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
        if (IsPlayer())
        {
            battleManager.PlayerInforCurr.statsPkm.spAtk -= spUp;
            isChangeSpAtk = false;
        }
        else
        {
            battleManager.EnemyInforCurr.statsPkm.spAtk -= spUp;
            isChangeSpAtk = false;

        }
        spUp = 0;
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
