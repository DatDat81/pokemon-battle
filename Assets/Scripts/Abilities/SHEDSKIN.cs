﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sau mỗi lượt, Pokémon này có 33% xác suất tự chữa trị PSN / BRN / PAR / FRZ / SLP và sẽ chữa trước khi nhận sát thương từ BRN / PSN.
/// </summary>
public class SHEDSKIN : BaseAbility,IAbility
{
    int random = 0;
    public void OnStartBattle(BattleManager battleManager)
    {

    }

    public void OnStartTurn(BattleManager battleManager)
    {

    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {
        random = Random.Range(1, 101);
        if(random<=33)
        {
            if(IsPlayer())
            {
                ShowMessage();
                battleManager.DataMng.player.PkmInforInBattle.StatusPkm = StatusPkmEnum.Normal;
                battleManager.UIFrameBattleMng.UpdateStatusPlayer(StatusPkmEnum.Normal);
            }
            else
            {
                battleManager.DataMng.enemy.PkmInforInBattle.StatusPkm = StatusPkmEnum.Normal;
                battleManager.UIFrameBattleMng.UpdateStatusPlayer(StatusPkmEnum.Normal);
            }
        }
    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
