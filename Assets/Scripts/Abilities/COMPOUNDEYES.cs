﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Accuracy của tất cả chiêu thức của Pokémon này tăng thêm 1.3 lần. Ví dụ chiêu thức có độ chính xác là 70% thì sẽ thành 91%. Nếu Pokémon này ở đầu đội hình, tỷ lệ gặp Pokémon hoang dã cầm vật phẩm tăng từ 50%/5%/1% lên đến 60%/20%/5%.
/// </summary>
public class COMPOUNDEYES : BaseAbility,IAbility
{
    bool isUpAccuracy = false;
    public void OnStartBattle(BattleManager battleManager)
    {
        if(isUpAccuracy==false)
        {
            isUpAccuracy = true;
            if(IsPlayer())
            {
                ShowMessage();
                for(int i=0;i<4;i++)
                {
                    if (battleManager.PlayerInforCurr.moveInfors[i] == null) continue;
                    battleManager.PlayerInforCurr.moveInfors[i].accuracy =Mathf.RoundToInt( battleManager.PlayerInforCurr.moveInfors[i].accuracy * 1.3f);
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    if (battleManager.EnemyInforCurr.moveInfors[i] == null) continue;
                    battleManager.EnemyInforCurr.moveInfors[i].accuracy = Mathf.RoundToInt(battleManager.EnemyInforCurr.moveInfors[i].accuracy * 1.3f);
                }
            }
        }
    }

    public void OnStartTurn(BattleManager battleManager)
    {

    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {
        if (isUpAccuracy == false)
        {
            isUpAccuracy = true;
            if (IsPlayer())
            {
                for (int i = 0; i < 4; i++)
                {
                    if (battleManager.PlayerInforCurr.moveInfors[i] == null) continue;
                    battleManager.PlayerInforCurr.moveInfors[i].accuracy = Mathf.RoundToInt(battleManager.PlayerInforCurr.moveInfors[i].accuracy * 1.3f);
                }
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    if (battleManager.EnemyInforCurr.moveInfors[i] == null) continue;
                    battleManager.EnemyInforCurr.moveInfors[i].accuracy = Mathf.RoundToInt(battleManager.EnemyInforCurr.moveInfors[i].accuracy * 1.3f);
                }
            }
        }
    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
