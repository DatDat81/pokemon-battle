﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactoryAbilities : Singleton<FactoryAbilities>
{
    IAbility ab;
    public IAbility CreateAbility(string abilityName, GameObject obj)
    {

        switch (abilityName)
        {
            case "GUTS":
                AddScript<GUTS>(obj);
                ab = obj.GetComponent<GUTS>();
                return ab;
            case "BIGPECKS":
                AddScript<BIGPECKS>(obj);
                ab = obj.GetComponent<BIGPECKS>();
                return ab;
            case "TANGLEDFEET":
                AddScript<TANGLEDFEET>(obj);
                ab = obj.GetComponent<TANGLEDFEET>();
                return ab;
            case "KEENEYE":
                AddScript<KEENEYE>(obj);
                ab = obj.GetComponent<KEENEYE>();
                return ab;
            case "COMPOUNDEYES":
                AddScript<COMPOUNDEYES>(obj);
                ab = obj.GetComponent<COMPOUNDEYES>();
                return ab;
            case "BLAZE":
                AddScript<BLAZE>(obj);
                ab = obj.GetComponent<BLAZE>();
                return ab;
            case "CHLOROPHYLL":
                AddScript<CHLOROPHYLL>(obj);
                ab = obj.GetComponent<CHLOROPHYLL>();
                return ab;
            case "OVERGROW":
                AddScript<OVERGROW>(obj);
                ab = obj.GetComponent<OVERGROW>();
                return ab;
            case "RAINDISH":
                AddScript<RAINDISH>(obj);
                ab = obj.GetComponent<RAINDISH>();
                return ab;
            case "SHEDSKIN":
                AddScript<SHEDSKIN>(obj);
                ab = obj.GetComponent<SHEDSKIN>();
                return ab;
            case "SHIELDDUST":
                AddScript<SHIELDDUST>(obj);
                ab = obj.GetComponent<SHIELDDUST>();
                return ab;
            case "SOLARPOWER":
                AddScript<SOLARPOWER>(obj);
                ab = obj.GetComponent<SOLARPOWER>();
                return ab;
            case "SWARM":
                AddScript<SWARM>(obj);
                ab = obj.GetComponent<SWARM>();
                return ab;
            case "TORRENT":
                AddScript<TORRENT>(obj);
                ab = obj.GetComponent<TORRENT>();
                return ab;
            default:
                Debug.LogError("Don't Exis Ability: " + abilityName);
                return null;
        }
    }
    void AddScript<T>(GameObject obj) where T : BaseAbility, IAbility
    {
        obj.AddComponent<T>();
    }
}
