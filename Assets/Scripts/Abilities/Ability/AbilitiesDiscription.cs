﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilitiesDiscription : Singleton<AbilitiesDiscription>
{
    // name Ability_Discription
    /// <summary>
    /// tat ca ability o dang Chữ Hoa
    /// </summary>
    private Dictionary<string, string> ablitiesDisctiption = new Dictionary<string, string>();

    public Dictionary<string, string> AblitiesDisctiption { get => ablitiesDisctiption; set => ablitiesDisctiption = value; }

    public string GetAbilityDiscriptionByName(string nameAbility)
    {
        if (nameAbility == "") Debug.LogError("Null Ability Name");
        string t;
        AbilitiesDiscription.Instance.AblitiesDisctiption.TryGetValue(nameAbility, out t);
        return t;
    }
}
