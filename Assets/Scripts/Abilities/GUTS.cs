﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUTS : BaseAbility,IAbility
{
    public void OnStartBattle(BattleManager battleManager)
    {

    }

    public void OnStartTurn(BattleManager battleManager)
    {

    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {

    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
