﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAbility 
{
    /// <summary>
    /// Bat dau vao Battle
    /// </summary>
    /// <param name="battleManager"></param>
    void OnStartBattle(BattleManager battleManager);

    /// <summary>
    /// khi bat dau vao turn cua pkm nay
    /// </summary>
    /// <param name="battleManager"></param>
    void OnStartTurn(BattleManager battleManager);

    /// <summary>
    /// truoc khi tung chieu
    /// </summary>
    /// <param name="battleManager"></param>
    void OnBeforeAttack(BattleManager battleManager);

    /// <summary>
    /// sau khi tung chieu
    /// </summary>
    /// <param name="battleManager"></param>
    void OnAfterAttack(BattleManager battleManager);

    /// <summary>
    /// sau khi bi tan cong
    /// </summary>
    /// <param name="battleManager"></param>
    void OnAfterBeAttacked(BattleManager battleManager);

    /// <summary>
    /// sau khi bi thay doi trang thai
    /// </summary>
    /// <param name="battleManager"></param>
    void OnAfterChangeStatus(BattleManager battleManager);

    /// <summary>
    /// khi pkm bi dinh mot don chi mang
    /// </summary>
    /// <param name="battleManager"></param>
    void OnGetCriticalHit(BattleManager battleManager);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="battleManager"></param>
    void OnEndTurn(BattleManager battleManager);
    
    /// <summary>
    /// khi pkm nay duoc thay vao(tran dau dang dien ra)
    /// </summary>
    /// <param name="battleManager"></param>
    void OnChangePokemonIn(BattleManager battleManager);

    /// <summary>
    /// khi pkm nay duoc thay ra(tran dau dang dien ra)
    /// </summary>
    /// <param name="battleManager"></param>
    void OnChangePokemonOut(BattleManager battleManager);

    /// <summary>
    /// khi pkm chet
    /// </summary>
    /// <param name="battleManager"></param>
    void OnDie(BattleManager battleManager);

    void OnEndBattle(BattleManager battleManager);
}
