﻿
/// <summary>
/// sau khi ra chieu 
/// </summary>
public interface IOnAfterAttack 
{
    void OnAfterAttack(BattleManager battleManager);
}
