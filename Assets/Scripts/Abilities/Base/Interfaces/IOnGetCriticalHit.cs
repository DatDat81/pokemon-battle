﻿/// <summary>
/// sau khi bi trung chieu tri mang
/// </summary>
public interface IOnGetCriticalHit
{
    void OnGetCriticalHit(BattleManager battleManager);
}
