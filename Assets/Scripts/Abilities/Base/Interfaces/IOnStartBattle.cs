﻿
/// <summary>
/// Bat dau vao Battle
/// 
/// </summary>
public interface IOnStartBattle 
{
    void OnStartBattle(BattleManager battleManager);
}
