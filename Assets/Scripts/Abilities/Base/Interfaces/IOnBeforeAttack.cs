﻿/// <summary>
/// truoc khi ra chieu
/// </summary>
public interface IOnBeforeAttack
{
    void OnBeforeAttack(BattleManager battleManager);
}
