﻿/// <summary>
/// khi bat dau vao turn cua pkm nay
/// </summary>
public interface IOnStartTurn 
{
    void OnStartTurn(BattleManager battleManager);
}
