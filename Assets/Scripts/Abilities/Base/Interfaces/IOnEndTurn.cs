﻿
/// <summary>
/// khi ket thuc turn
/// </summary>
public interface IOnEndTurn 
{
    void OnEndTurn(BattleManager battleManager);
}
