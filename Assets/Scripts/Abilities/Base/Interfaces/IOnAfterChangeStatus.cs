﻿/// <summary>
/// sau khi bi thay doi trang thai
/// </summary>
public interface IOnAfterChangeStatus
{
    void OnAfterBeChangedStatus(BattleManager battleManager);
}
