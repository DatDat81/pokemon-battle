﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAbility : MonoBehaviour
{
    protected bool IsPlayer()
    {
        if (gameObject.tag.CompareTo("Player")==0)
            return true;
        return false;
    }    

    protected void ShowMessage()
    {
        string show ="Ability/HidenAbility "+this.GetType().ToString() + ": " + AbilitiesDiscription.Instance.GetAbilityDiscriptionByName(this.GetType().ToString());
        UIFrameBattleMng.Instance.ShowNoty(show);
    }
}
