﻿
using UnityEngine;
/// <summary>
/// Đối phương không thể giảm Accuracy của Pokémon này. Hiệu ứng này không áp dụng cho các đặc tính ảnh hưởng trực tiếp tới Accuracy như Wonder Skin và Sand Veil. Pokémon này khi tấn công sẽ bỏ qua Evasiveness của đối phương.
/// </summary>
public class KEENEYE : BaseAbility,IAbility
{
    public void OnStartBattle(BattleManager battleManager)
    {
        if(IsPlayer())
        {
            battleManager.DataMng.player.PkmInforInBattle.CanReduceAccuracy = false;
        }
        else
        {
            battleManager.DataMng.enemy.PkmInforInBattle.CanReduceAccuracy = false;
        }
    }

    public void OnStartTurn(BattleManager battleManager)
    {
        Debug.Log("Keenee");
    }

    public void OnBeforeAttack(BattleManager battleManager)
    {
    }

    public void OnAfterAttack(BattleManager battleManager)
    {

    }

    public void OnAfterBeAttacked(BattleManager battleManager)
    {

    }

    public void OnAfterChangeStatus(BattleManager battleManager)
    {

    }

    public void OnGetCriticalHit(BattleManager battleManager)
    {

    }

    public void OnEndTurn(BattleManager battleManager)
    {
    }

    public void OnChangePokemonIn(BattleManager battleManager)
    {
        if (IsPlayer())
        {
            battleManager.DataMng.player.PkmInforInBattle.CanReduceAccuracy = false;
        }
        else
        {
            battleManager.DataMng.enemy.PkmInforInBattle.CanReduceAccuracy = false;
        }
    }
    public void OnChangePokemonOut(BattleManager battleManager)
    {
    }

    public void OnDie(BattleManager battleManager)
    {

    }
    public void OnEndBattle(BattleManager battleManager)
    {

    }
}
