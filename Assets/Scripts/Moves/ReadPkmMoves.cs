﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReadPkmMoves : MonoBehaviour
{
    public TextAsset moveDt;
    void Start()
    {
        string[] split = moveDt.ToString().Split('\n');
        for(int i=0;i<split.Length;i++)
        {
            string[] s = split[i].Split(',');
            MoveInfor mv = new MoveInfor();
            mv.name = s[1].TrimEnd();
            int.TryParse(s[4].TrimEnd(), out mv.power);
            mv.typeMove = TypePkm_DT.Instance.ConvertTypePkmToEnum(s[5].TrimEnd());
            mv.category = AllListMoves.Instance.ConvertStringToCategory(s[6].TrimEnd());
            int.TryParse(s[7].TrimEnd(), out mv.accuracy);
            int.TryParse(s[8].TrimEnd(), out mv.pp);
            int.TryParse(s[9].TrimEnd(), out mv.sideEffect);

            string dis="";
            for(int k=13;k<s.Length;k++)//ghep cac discription bi tach boi dau: ,
            {
                dis += s[k].TrimEnd();
            }
            mv.discription = dis;

            AllListMoves.Instance.AllMove.Add(mv.name, mv);
        }
    }
}
