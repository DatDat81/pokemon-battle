﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoveInfor 
{
    public string name="";
    public int power; // dung trong cong thuc tinh Damage
    public TypePokemonEnum typeMove;
    public CategoryMoveEnum category;
    public int accuracy;
    public int pp; // so lan co the dung
    public int sideEffect;// hieu ung phu
    public string discription;

    public void SetUpData(MoveInfor moveInfor)
    {
        name = moveInfor.name;
        power = moveInfor.power;
        typeMove = moveInfor.typeMove;
        category = moveInfor.category;
        accuracy = moveInfor.accuracy;
        pp = moveInfor.pp;
        sideEffect = moveInfor.sideEffect;
        discription = moveInfor.discription;
    }
}
