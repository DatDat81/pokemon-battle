﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// content All Move of All pkm
/// </summary>
public class AllListMoves : Singleton<AllListMoves>
{
    private Dictionary<string, MoveInfor> allMove = new Dictionary<string, MoveInfor>();

    public Dictionary<string, MoveInfor> AllMove { get => allMove; set => allMove = value; }

    public CategoryMoveEnum ConvertStringToCategory(string category)
    {
        switch(category)
        {
            case "Physical":
                return CategoryMoveEnum.Physical;
            case "Special":
                return CategoryMoveEnum.Special;
            case "Status":
                return CategoryMoveEnum.Status;
        }
        return CategoryMoveEnum.Physical;
    }

    public MoveInfor GetMoveInfor(string nameMove)
    {
        MoveInfor mv = new MoveInfor();
        allMove.TryGetValue(nameMove, out mv);
        return mv;
    }

    public bool CheckMoveSameTypeWithPkm(PkmInfor pkm, string nameMove) // use in Caculation damage 
    {
        MoveInfor move = GetMoveInfor(nameMove);
        for(int i=0;i<pkm.typePkm.TypePokemon.Count;i++)
        {
            if (move.typeMove == pkm.typePkm.TypePokemon[i]) return true;
        }    
        return false;
    }
}
