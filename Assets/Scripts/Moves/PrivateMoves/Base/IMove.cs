﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMove 
{
    void DoPhysical(BattleManager battleManager);
    void DoSpecial(BattleManager battleManager);
    void DoStatus(BattleManager battleManager);
}
